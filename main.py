# Python program to explain os.path.isfile() method

# importing os module
import os
import logging
import re
import concurrent.futures
# from PyPDF2 import PdfFileMerger, PdfFileReader
from pdfrw import PdfReader, PdfWriter
from shutil import copyfile

logging.basicConfig(filename='digitalizacion.log',filemode='w',level=logging.DEBUG, format='%(threadName)s: %(message)s')
#logging.basicConfig(level=logging.DEBUG, format='%(threadName)s: %(message)s')

# directorio_origen= "D:/digitalizacion"
# directorio_destino= "D:/destino"
#
directorio_origen= "/mnt/storage/ftp/OficinasOK/Hermosillo/Parte1"
directorio_destino= "/mnt/storage/ftp/RespaldonuevoCatastro"





def evalua_existencia_directorio(pdf_filename):
    # Un split sobre el nombre del archivo
    filename_splitted = pdf_filename[0].split('-')

    if len(filename_splitted) != 5:
        logging.warning("WARNING: El nombre del archivo %s no cumple el formato obligatorio de clave catastral" % (pdf_filename))
        return
    num_municipio = filename_splitted[0]
    num_oficina = filename_splitted[1]
    num_carta_or_region = filename_splitted[2]
    num_manzana_or_cuadrante = filename_splitted[3]
    num_predio = filename_splitted[4].replace('.pdf', '')
    es_rural=False
    if re.search('[a-zA-Z]', num_carta_or_region):
        es_rural = True

    #valida formato correcto del nombre del archivo
    #cuando la clave es rural y urbana
    if ((es_rural==True \
        and len(num_municipio)==2 \
        and len(num_oficina)==4 \
        and len(num_carta_or_region)==3 \
        and len(num_manzana_or_cuadrante)==1 \
        and len(num_predio)==4) \
        or \
        (es_rural==False \
         and len(num_municipio) == 2 \
         and len(num_oficina) == 4 \
         and len(num_carta_or_region) == 2 \
         and len(num_manzana_or_cuadrante) == 3 \
         and len(num_predio) == 3)):

        #arma el path donde debería existir el directorio
        path_to_find= '%s/%s/%s/%s/%s' % (directorio_destino,num_municipio,num_oficina,num_carta_or_region,num_manzana_or_cuadrante)
        nombre_pdf= pdf_filename[0]

        #boleano que dice si existe o no el directorio
        existeDirectorio = os.path.isdir(path_to_find)
        if existeDirectorio:
            # logging.info("si existe ruta")
            #momento de evaluar el archivo
            escribe_archivo_txt(num_municipio, num_oficina, num_carta_or_region, num_manzana_or_cuadrante,num_predio)
            evalua_existencia_archivo(path_to_find,nombre_pdf)
        else:
            ##se crea el directorio
            while 1 == 1:
                if os.path.isdir('%s/%s/' % (directorio_destino,num_municipio)):
                    if os.path.isdir('%s/%s/%s' % (directorio_destino, num_municipio, num_oficina)):
                        if os.path.isdir('%s/%s/%s/%s' % (directorio_destino, num_municipio, num_oficina, num_carta_or_region)):
                            if os.path.isdir('%s/%s/%s/%s/%s' % (directorio_destino, num_municipio, num_oficina, num_carta_or_region, num_manzana_or_cuadrante)):
                                # logging.info('Ruta completada')
                                break
                            else:
                                try:
                                    os.mkdir('%s/%s/%s/%s/%s' % (directorio_destino, num_municipio, num_oficina, num_carta_or_region, num_manzana_or_cuadrante))
                                    logging.info('SUCCESS: Directorio manzana/cuadrante creado')
                                    break
                                except OSError:
                                    logging.Error("ERROR: Creacion de directorio manzana/cuadrante falló")
                        else:
                            try:
                                os.mkdir('%s/%s/%s/%s' % (directorio_destino, num_municipio, num_oficina, num_carta_or_region))
                                logging.info('SUCCESS: Directorio carta/region creado')

                            except OSError:
                                logging.Error("ERROR: Creacion de directorio carta/region falló")
                    else:
                        try:
                            os.mkdir('%s/%s/%s' % (directorio_destino, num_municipio, num_oficina))
                            logging.info('SUCCESS: Directorio oficina creado')

                        except OSError:
                            logging.Error("ERROR: Creacion de directorio oficina catastral falló")
                else:
                    try:
                        os.mkdir('%s/%s/' % (directorio_destino,num_municipio))
                        logging.info('SUCCESS: Directorio municipio creado')

                    except OSError:
                        logging.Error("ERROR: Creacion de directorio municipio falló")

            escribe_archivo_txt(num_municipio, num_oficina, num_carta_or_region, num_manzana_or_cuadrante,num_predio)
            evalua_existencia_archivo(path_to_find,nombre_pdf)

    else:
        logging.warning("WARNING: El nombre del archivo %s no cumple el formato obligatorio de clave catastral" % (pdf_filename[0]))
        return



def evalua_existencia_archivo(ruta_archivo,nombre_pdf):
    for root, dirs, files in os.walk(ruta_archivo):
        # si el nombre del pdf es encontrado en el array de los files ya existentes se realiza un merge de pdfs
        path_archivo_destino =ruta_archivo + "/" + nombre_pdf
        path_archivo_origen = directorio_origen + "/" + nombre_pdf

        if nombre_pdf in files:
            try:
                # merger = PdfFileMerger()
                writer = PdfWriter()

                # El orden es primero el de origen y luego destino :(
                for inpfn in [path_archivo_origen,path_archivo_destino ]:
                    writer.addpages(PdfReader(inpfn,verbose=False).pages)

                outfn = ruta_archivo + "/tmp_" + nombre_pdf
                writer.write(outfn)
                logging.info('SUCCESS: Archivo %s fusionado ' % (nombre_pdf))


            except Exception as err:
                # logging.error('')
                logging.error('ERROR: Error al fusionar los archivos: %s' % (err))
            finally:
                if os.path.isfile(ruta_archivo + "/tmp_" + nombre_pdf):
                    os.remove(path_archivo_destino)
                    os.rename(ruta_archivo + "/tmp_" + nombre_pdf,path_archivo_destino)

        else:
                copyfile(path_archivo_origen, path_archivo_destino)
                logging.info('SUCCESS: Archivo %s copiado ' % (nombre_pdf))

def escribe_archivo_txt(num_municipio, num_oficina, num_carta_or_region, num_manzana_or_cuadrante,num_predio):
    f = open("data.txt", "a")
    f.write('%s,%s,%s,%s,%s\n' % (num_municipio, num_oficina, num_carta_or_region, num_manzana_or_cuadrante,num_predio))
    f.close()
    return

def leer_origen(filename):
    # Obtenemos el path del archivo
    path = filename
    # Realizamos split en el path
    path_splitted = path.split('/')
    # buscamos dentro del array coincidencia de .pdf para solo obtener el nombre del archivo
    pdf_filename = [s for s in path_splitted if ".pdf" in s]
    # metodo que evalua y crea directorios
    evalua_existencia_directorio(pdf_filename)
    # en este punto los directorios existen
    # return True











if __name__ == '__main__':
    # with concurrent.futures.ThreadPoolExecutor() as executor:
        #obtiene todos los archivos del directorio de origen
        for root, dirs, files in os.walk(directorio_origen):
            futures = []
            contador=0
            # loopea cada archivo y crea threads
            for filename in files:
                leer_origen(filename)
                # futures.append(executor.submit(leer_origen,filename))
                contador += 1
                print('INFO: %s clave %s' % (contador,filename))

            # print('INFO: Se procesaron un total de %s archivos' % (contador))
            # for future in concurrent.futures.as_completed(futures):
            #     print(future.result())








