# FUSION DIGITALIZACION

Script que ordena los archivos de digitalización y fusiona archivos pdf cuando encuentra que en el directorio destino ya existe previamente este archivo pdf.

## Installation

Clone the repository

Create a virtual environment with venv

Configure your directory of source and destination

```bash
directorio_origen= "/mnt/storage/ftp/Hermosillo/Parte1"
directorio_destino= "/mnt/storage/ftp/RespaldonuevoCatastro"
```

## Usage

```python
python main.py
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
